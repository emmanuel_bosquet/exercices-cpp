#include <iostream>
#include <random>

using namespace std;

int main() {

  std::random_device rd;  // obtain a random number from hardware
  std::mt19937 gen(rd()); // seed the generator
  std::uniform_int_distribution<> distr(1, 100); // define the range

  int random_number = distr(gen);

  // to debug only, this should be commented
  //   cout << random_number << endl;

  // instantiate the variable, it will be mutated each time the user guesses
  int user_guess;

  for (;;) {
    cout << "guess a number:";
    cin >> user_guess;

    // detect wrong input
    if (cin.fail()) {
      cout << "bad input!" << endl;
      cin.clear();
      cin.ignore();
    }
    // handle good input
    else {
      if (user_guess < random_number) {
        cout << "Too low, try higher" << endl;
      } else if (user_guess > random_number) {
        cout << "Too high, try lower" << endl;
      } else {
        cout << "Congrats!" << endl;
        break;
      }
    }
  }
}