#include <fstream>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>

using namespace std;

int main() {
  ifstream myfile("../dictionnaire_ascii");

  // declare the "line" variable, of type string

  string line;
  string concatenated;

  // iterate over lines with getline() and concatenate them all with append()
  if (myfile.is_open()) {
    while (getline(myfile, line)) {
      concatenated.append(line);
    }
    myfile.close();
  }

  // 3 million UTF8 characters in one line!
  // cout << concatenated << endl;

  // The keys are characters
  // the values are counters
  map<char, int> hashmap;
  map<char, int>::iterator iterator;

  for (const char &character : concatenated) {
    // search for the character in the hashmap
    iterator = hashmap.find(character);

    // If the character is present...
    if (iterator != hashmap.end()) {
      // increment the counter
      hashmap[character] += 1;
    }
    // if not present...
    else {
      // insert the character as key and set the counter to 1
      hashmap.insert(pair<char, int>(character, 1));
    }
  }

  // display the hash map using the iterator
  for (auto iterator = hashmap.cbegin(); iterator != hashmap.cend();
       ++iterator) {
    cout << "lettre " << iterator->first << ": " << iterator->second << endl;
  }
}