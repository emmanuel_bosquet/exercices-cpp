#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

using namespace std;

void output_byte_as_ASCII(char character) {
  if (character < 32 && 127 > character) {
    cout << '.';
  } else {
    cout << character;
  }
}

void output_byte_as_hex(char character) {
  cout << setw(2) << setfill('0')
       << std::hex
       // black magic from stack overflow to make sure a character displays
       // properly on any platform
       << static_cast<int>(static_cast<unsigned char>(character)) << " ";
}

void display_one_line_of_data(char *data, int address) {

  // hexadecimal address
  // like the util-linux, 8-character hex addresses (ex: "0000010")
  cout << setw(8) << std::setfill('0') << std::hex << address;

  // double space
  cout << "  ";

  // display the hex values of each byte
  for (int h = 0; h < 16; h++) {
    // additional space in order to have to columns of eight hex values each
    if (h == 8) {
      cout << " ";
    }
    output_byte_as_hex(data[h]);
  }

  // additional space
  cout << " ";

  // print the ASCII part
  cout << "|";
  for (int i = 0; i < 16; i++) {
    output_byte_as_ASCII(data[i]);
  }
  cout << "|" << endl;
}

// let's rewrite "hexdump -C" in C++.
// two arguments (argc=2): the name of the program, and the meaningful argument
int main(int argc, char *argv[]) {
  cout << "your argument is: " << argv[1] << endl;

  // open the file passed as an argument
  ifstream myfile(argv[1]);

  if (!myfile.good()) {
    cout << "bad input" << endl;
    return 1;
  }

  // create a buffer
  const int BUFFER_SIZE = 16;
  vector<char> buffer(BUFFER_SIZE, 0);
  int address = 0;

  while (1) {
    myfile.read(buffer.data(), BUFFER_SIZE);

    display_one_line_of_data(buffer.data(), address);

    address += BUFFER_SIZE;
    if (!myfile)
      break;
  }
}