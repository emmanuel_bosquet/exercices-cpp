# Cours de C++

## Qu'est ce que c'est ?

[cf wikipedia](https://fr.wikipedia.org/wiki/C%2B%2B)

Créé initialement par Bjarne Stroustrup dans les années 1980, `C++` est un langage de programmation compilé permettant la programmation sous de multiples paradigmes, dont la programmation procédurale, la programmation orientée objet et la programmation générique. Ses bonnes performances, et sa compatibilité avec le C en font un des langages de programmation les plus utilisés dans les applications où la performance est critique.

Il est compilé c'est-à-dire que sont code source doit êre transformer en code machine executable puis il peut être executé à vonlonté. L'executable générer est autosuffisant et n'a pas besoin d'un autre programme à l'instar des langague interprété (javascript, php, python, etc) ou bytecode (java, c#, etc).

Le C++ est un langage complexe très permissif (il est facile de faire des erreurs fatal) si bien que la plus part des recommendation est d'en utiliser qu'une sous partie comme les [recommandations google sur le C++](https://google.github.io/styleguide/cppguide.html). Ici nous allons voir les bases non exaustives du C++ vous permettant de comprendre du code existant et d'ecrire des programmes ou bibliothèques.

Pour ce cours je recommande d'utiliser l'IDE QtCreator, d'autre éditeur de code peuvent faire l'affaire notemment vscode ou visual studio pour windows et xcode pour mac.
La suite du cours se portant sur la création d'interface graphique avec Qt, QtCreator est une bonne solution grâce à sont éditeur graphique.

## Liens Documentations

- https://en.cppreference.com/w/
- https://www.cplusplus.com/doc/tutorial/

## Hello world

Dans QtCreator créer un nouveau projet non QT selectionner Plain C++ application. Vous devriez avoir votre premier programme `main.cpp` :

```c++
// main.cpp
#include <iostream>

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    return 0;
}

```

Lancer la compilation et l'execution du project avec le bouton play vert en bas à gauche de QtCreator ou avec le racourcie `Ctrl+R`. En bas de l'éditeur un panel doit s'ouvrir affichant

```
Hello World!
```

ajouter à entre la ligne `cout` et `return`

```c++
    std::cerr << "un message d'erreur " << 42 << std::endl;
```

Réexecuté, le message d'erreur apparait en rouge.

Changer le nombre après `return` et constaté le changement dans la sortie.

0 signifie au système d'exploitation que l'execution c'est bien déroulé, n'importe quelle autre nombre pour signaler une erreur.

### Etude du hello world

```c++
#include <iostream>
```

Inclue d'autre fichier C++ `<>` pour les fichier de la librairy standard ou installer dans le système. `""` pour des fichiers du projet path relatif à `main.cpp`

`iostream` déclare `cout`, `cerr`, `cin`, `endl` et d'autre fonctionnalité pour la gestion des entrée/sortie.

```c++
using namespace std;
```

déclare que tout ce qui se trouve dans l'espace de nom `std` peut être utiliser sans préfixer par `std::`, sans cette ligne il faut changer la ligne du `cout` en

```c++
    std::cout << "Hello World!" << std::endl;
```

`cout` et `cerr` permettent d'écrire dans la console en sortie standart ou en sortie d'erreur. `endl` ajoute une fin de ligne, c'est équivalent à `'\n'` + std::flush().

```c++
int main()
```

déclare une fonction nommé `main` sans paramètre `()` retournant un entier `int`.

```c++
    return 0;
```

retourne l'entier `0` à la fin de l'execution de la fonction `main`.

```c++
{
}
```

les accolades définissent un block de code, ici le corps de la fonction `main`. On appele aussi ces blocks de code `scope`.

## Les commentaires

```c++
// un commentaire sur une seul ligne
std::cout << "coucou" << std::endl; // un commentaire en fin de ligne
/*
  un commentaire
  sur plusieur
  ligne
*/
```

## Les type de base

Les taille des différents type de nomnre en C++ peuvent varier suivant l'architecture processeur. De nos jours c'est principalement 32 ou 64bit. Des alias sont définit ou la taille en bit des nombres est explicite dans le nom. La taille est en octet que prend chaque type en mémoire.

| Type                     | alias      | Taille | Range                                   |
| ------------------------ | ---------- | -----: | --------------------------------------- |
| `short int`              | `int16_t`  |      2 | -32,768 to 32,767                       |
| `unsigned short int`     | `uint16_t` |      2 | 0 to 65,535                             |
| `int`                    | `int32_t`  |      4 | -2,147,483,648 to 2,147,483,647         |
| `unsigned int`           | `uint32_t` |      4 | 0 to 4,294,967,295                      |
| `long int`               | `int32_t`  |      4 | -2,147,483,648 to 2,147,483,647         |
| `unsigned long int`      | `uint32_t` |      4 | 0 to 4,294,967,295                      |
| `long long int`          | `int64_t`  |      8 | -2^63 to 2^63-1                         |
| `unsigned long long int` | `uint64_t` |      8 | 0 to 18,446,744,073,709,551,615         |
| `signed char`            | `int8_t`   |      1 | -128 to 127                             |
| `unsigned char`          | `uint8_t`  |      1 | 0 to 255                                |
| `float`                  |            |      4 | nombre à virgule flottante sur 32 bits  |
| `double`                 |            |      8 | nombre à virgule flottante sur 64 bits  |
| `long double`            |            |     12 | nombre à virgule flottante sur 128 bits |

Pour connaitre la taille d'un type on utilise `sizeof(type)`. Il existe des types qui dépandent du processeurs notemment `size_t` qui prend la taille maximal supporté part celui-ci, on l'utilise souvant pour les indices des tableaux.

```c++
    cout << sizeof(size_t) << " " << sizeof(int) << endl;
```

Le type `char` est utiliser pour définir un caractère, un tableau de `char` pour une chaine de caractère. Un caractère n'est rien d'autre que des nombres. La table de corespondance la table ASCII :

![table ascii](https://www.asciitable.com/asciifull.gif)

Il existe aussi le type `bool` représantant des booléens prenant les valeur `true` ou `false`.

Il type `void` est un type particulier qui ne correspond à aucun type, il est souvant utilisé comme type de retour d'une fonction ou d'une méthode ne retournant aucune valeur.

## déclaration de variable

Déclarer une variable signale au compilateur de réserver une place mémoire pour y affecter valeur.

```c++
    int ma_variable;
    int a1, a2, a3;
    int b1 = 1, b2 = 2;

    const int c = 42;
```

Attention ces variables sont définit mais non initialisé leurs valeurs n'est pas définit, elles peuvents change d'une execution à l'autre.

```c++
    const int constante = 42; // ne peut être modifier

    int nombre; // déclaration
    cout << "avant initialisation " << nombre << endl;
    nombre = 35; // initialisation en affectant la valeur 35;
    cout << "après initialisation " << nombre << endl;

    int autre = 34;
    cout << "initialisation avec la déclaration " << autre << endl;

    int res = nombre + autre;
    cout << "initialisation avec la déclaration avec calcul " << res << endl;

    char a = 'a';
    char b = 98;
    char c = a + 2;
    cout << "character " << a << b << c << endl;

    int from_hex = 0xa6; // ou 0xA6 => 255 en décimale
    cout << "0xff en décimale " << from_hex << endl;

    int from_binary = 0b1101; // 2^3 + 2^2 + 2^0 = 8 + 4 + 1 = 13
    cout << "0b1101 en décimale " << from_binary << endl;

    int from_octal = 0766;
    cout << "0766 en décimale " << from_octal << endl;

    float f = 3.14159;
    double d =  1.79769e-3; // notation scientifique
    cout << "nombre à virgule " << f << '\t' << d << endl;
```

Les scopes permettent de limiter la porté d'une déclaration

```c++
    int a = 1;
    {
        int b = a + 1; // on peut accéder à `a` et `b`
        cout << a << " " << b << endl;
    }
    // erreur: b n'existe plus
    cout << a << " " << b << endl;
```

On peut redifinir une varaibles défini dans un scope parent la variable ne sera plus accessible avant la fin du scope, la varaible redéfinir porte le même nom mais c'est n'est pas le même emplacement dans la mémoire.

```c++
    int age = 42;
    cout << "avant scope " << age << endl;
    {
        cout << "debut scope " << age << endl;
        int age = 4;
        cout << "redefini " << age << endl;
    }
    cout << "après scope " << age << endl;
```

## cast



## entée standard

https://www.cplusplus.com/doc/tutorial/basic_io/

Demander à l'utilisateur rentrer des donnée en console

```c++
cout << "Quelle est votre age ? ";
int age;
cin >> age;
cout << "votre age est de " << age << endl;
```

## Opération arithmétique

| Opérateur                   | description                                           |
| --------------------------- | ----------------------------------------------------- |
| `--` `++`                   | incrémente de 1 ou décrémente de 1                    |
| `+` `-` `*` `/` `%`         | arithmetique attention au type 1/2 = 0; 1.0/2.0 = 0.5 |
| `<` `<=` `>` `>=` `==` `!=` | comparaisons                                          |
| `\|\|` `&&` `!`             | et, ou, not logique                                   |
| `&` `\|` `<<` `>>` `~` `^`  | et, ou, lshift, rshift, complement, xor binaire       |

```c++
    int a = 1;
    cout << "a:" << a << endl;
    int b = a++; // affect a à b puis incrémente a
    cout << "a:" << a << " b:" << b << endl;
    int c = ++a; // incrémente a puis affecte a à b
    cout << "a:" << a << " c:" << c << endl;
```

```c++
    cout << "2 / 3 = " << (2 / 3) << endl;
    cout << "2.0 / 3.0 = " << (2.0 / 3.0) << endl;
    cout << "5 / 2 = " << (5 / 2) << endl;
    cout << "5.0 / 2.0 = " << (5.0 / 2.0) << endl;
```

## Condition et boucles

### if

```c++
    bool condition = true;
    if (condition)
    {
        cout << "vrai" << endl;
    }

    condition = false;
    if (condition)
    {
        cout << "vrai" << endl;
    }
    else
    {
        cout << "faux" << endl;
    }

    int n = 5;
    if (n < 5)
    {
        cout << "n < 5" << endl;
    }
    else if (n < 10)
    {
        cout << "5 < n < 10" << endl;
    }
    else
    {
        cout << "10 < n" << endl;
    }


    if (5 == 5 && 12 != 5)
    {
        cout << "vrai" << endl;
    }


    if (5 == 2 || 12 != 5)
    {
        cout << "vrai" << endl;
    }
```

### for

```c++
    for (int i = 0; i < 10; i++)
    {
        cout << i << endl;
    }

    cout << " ------ " << endl;

    bool found = false;
    for (int i = 5; i >= 0 || !found; i--)
    {
        cout << i << endl;
        if (i == 2)
            found = true;
    }
    cout << " ------ " << endl;

    for (int i = 5; i >= 0; i--)
    {
        cout << i << endl;
        if (i == 2)
            break; // stop
    }
    cout << " ------ " << endl;

    for (int i = 0; i < 10; i++)
    {
        if (i % 2 == 1)
        {
            continue; // skip
        }
        cout << i << endl;
    }
```

### while

```c++
int i = 10;
while (i > 0)
{
    cout << i << endl;
    i -= 2;
}

int j = 0;
do {
    cout << j << endl;
} whild (j < 5);
```

### switch

Tester d'autre valeur de `n`

```c++
    int n = 1;
    switch (n)
    {
    case 0:
        cout << "match 0" << endl;
        break;

    case 1:
        cout << "math 1 mais continue de matcher" << endl;
    case 2:
    case 3:
        cout << "match 1,2 ou 3" << endl;
        break;

    default:
        cout << "ne match aucune valeur" << endl;
    }
```

### boucles infinies

```c++
for ( ; ; )
{
    cout << "arrete moi si tu peux !" << endl;
}
```

## tableau

```c++
int arr1[10];
int n = 10;
int arr2[n];
int arr[] = { 10, 20, 30, 40 };
arr[2] = 420;
cout << arr[0] << " " << arr[2] << endl;
```

multi-dimension

```c++
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    const int width = 5;
    const int height = 3;
    int grid[height][width];
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            grid[y][x] = y*width + x;
        }
    }

    cout << "y x:" << endl;
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            cout << setfill(' ') << setw(3) << grid[y][x] << " ";
        }
        cout << endl;
    }

    cout << "x y:" << endl;
    for (int x = 0; x < width; x++)
    {
        for (int y = 0; y < height; y++)
        {
            cout << setfill(' ') << setw(3) << grid[y][x] << " ";
        }
        cout << endl;
    }
    return 0;
}




```

## Les pointeurs et références

Un pointeur `*` est une variable contenant l'adresses d'une autre variable.

```c++
    int val = 42;
    int foo = 1;

    int *ptr = nullptr;
    cout << "ptr " << ptr << endl;

    ptr = &val; // & pour prendre l'adresse de val;
    cout << "ptr=" << ptr << "  *ptr=" << *ptr << "  val=" << val << endl;
    val = 10;
    cout << "ptr=" << ptr << "  *ptr=" << *ptr << "  val=" << val << endl;
    *ptr += 5;
    cout << "ptr=" << ptr << "  *ptr=" << *ptr << "  val=" << val << endl;
    ptr = &foo;
    cout << "ptr=" << ptr << "  *ptr=" << *ptr << "  val=" << val << endl;
```

Les pointeurs constant ne peuvent changer d'adresses mais ils peuvent changer la variable sur laquelle ils pointent.

```c++
int x = 1;
      int *       p1 = &x;  // non-const pointer to non-const int
const int *       p2 = &x;  // non-const pointer to const int
      int * const p3 = &x;  // const pointer to non-const int
const int * const p4 = &x;  // const pointer to const int 
```

Les références sont des pointeurs qui ne peuvent être null et ne peuvent changer leurs adresses.

```c++
    int val = 42;
    int foo = 1;

    int &ref = val;
    cout << "&ref=" << &ref << "  ref=" << ref << "  val=" << val << endl;
    val = 10;
    cout << "&ref=" << &ref << "  ref=" << ref << "  val=" << val << endl;
    ref += 5;
    cout << "&ref=" << &ref << "  ref=" << ref << "  val=" << val << endl;
    ref = foo;
    cout << "&ref=" << &ref << "  ref=" << ref << "  val=" << val << endl;
```

## Allocation mémoire dynamique

Jusqu'ici les valeurs des variables sont local au scope où elles sont déclarées et n'existe plus à la sortie du scope qu'on appele la `stack` (pile en français). Une autre partie de la mémoire nommé `heap` (tas en français). Elle permet de stoquer des valeurs de taille arbitraire voir variable et n'est limité que par la quantité de mémoire de l'ordinateur.

```c++
    int *ptr = new int(42); // alloue 1 int sur le tas, lui affecte la valeur 42  et retourne sont adresse
    cout << ptr << "  " <<  *ptr << endl;

    delete ptr; // rend le mémoire alloué
    // si vous l'oublier on appelle ca une fuite de mémoire
```

```c++
    int *tableau = new int[10];
    for (int i = 0; i < 10; i++)
    {
        tableau[i] = i * i;
    }

    for (int i = 0; i < 10; i++)
    {
        cout << i << "² = " << tableau[i] << endl;
    }
    delete[] tableau;
```

## Fonctions

L'ordre des déclarations importante, on ne peut utiliser une fonction qui n'as pas encore été déclarer.

```c++
#include <iostream>

void maFonction()
{
    std::cout << "coucou de maFonction" << std::endl;
}

int main()
{
    maFonction();
    maFonction();
    return 0;
}
```

```c++
#include <iostream>

// déclaration uniquement
void maFonction();

int main()
{
    maFonction();
    maFonction();
    return 0;
}

void maFonction()
{
    std::cout << "coucou de maFonction" << std::endl;
}
```

Paramètres et type de retour

```c++
int add(int a, int b)
{
    return a + b;
}

std::cout << add(5,6) << std::endl;
```

Pour retourner plusieurs valeurs passer les elements par pointeurs ou références

```c++
void out(int p0, int *out1, int &out2)
{
    *out1 = p0 + 1; // modifie out1 par un pointeur
    out2 = p0 + 2; // modifie out2 par référence
}
int main()
{
    int res1 = 0;
    int res2 = 0;
    out(5, &res1, res2);
    std::cout << res1 << " " << res2 << std::endl;    
    return 0;
}

```

## string

En C les string sont représenté par un tableau de char dont le dernier caractère est `'\0'` (0).

```c++
#include <string>
//...

    char *text = "bonjours !"; // pointeur vers le début de la chaine de caractère
    cout << text << endl;
    char *jours = text + 3;  // on point vers l'interieur de la "bonjours" de 3 caracètres
    cout << jours << endl;

    // std::string str = "coucou";
    string str = "coucou";
    cout << str <<  " " << str.length() << endl;
    str[0] = 'C';
    cout << str <<  " " << str.length() << endl;
    str.append(" comment ca va ?");
    cout << str <<  " " << str.length() << endl;
    str += " au top je fais du c++ !";
    cout << str <<  " " << str.length() << endl;
    str.clear();
    cout << str <<  " " << str.length() << endl;
```

`std::string` est une classe standard C++ qui abstrait les chaines de caractère, permet le redimentionnement, la manipulation etc.

## struct

```c++
#include <iostream>
using namespace std;

struct Point {
    int x;
    int y;
};

Point add(Point a, Point b)
{
    return Point{a.x + b.x, a.y + b.y};
}

int main()
{
    Point p;
    p.x = 10;
    p.y = 20;

    cout << "p : " << p.x << ',' << p.y << endl;

    Point p1 { 1, 2 };
    cout << "p1: " << p1.x << ',' << p1.y << endl;

    Point p2 = add(p, p1);
    cout << "p2: " << p2.x << ',' << p2.y << endl;

    Point *p_ptr = &p;
    cout << "p_ptr: " << p_ptr->x << ',' << p_ptr->y << endl;

    return 0;
}

```

## std::vector

`std::vector` est une classe de tableau dynamique.

```c++
#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> v;
    cout << "cap:" << v.capacity() << " size:" << v.size() << endl;
    for (int i = 0; i < 10; ++i)
    {
        v.push_back(i);
        cout << "cap:" << v.capacity() << " size:" << v.size() << endl;
    }

    for (int i = 0; i < 10; ++i)
    {
        cout << v[i] << endl;
    }
    return 0;
}

```

## lire/écrire dans des fichiers

https://www.cplusplus.com/doc/tutorial/files/

Ecrire

```c++
// basic file operations
#include <iostream>
#include <fstream>
using namespace std;

int main () {
  ofstream myfile;
  myfile.open("example.txt");
  // myfile.open ("example.bin", ios::out | ios::app | ios::binary);
  myfile << "hello world.\n";
  myfile << "hello c++.\n";
  myfile.close();
  return 0;
}
```

Lire

```c++
// reading a text file
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main () {
  string line;
  ifstream myfile("example.txt");
  if (myfile.is_open())
  {
    while ( getline (myfile,line) )
    {
      cout << line << endl;
    }
    myfile.close();
  }

  else cout << "Unable to open file"; 

  return 0;
```

## Classe

Les Classes sont le fondement de la programmation Orienté Object.

Une `class` est comme une `struct` auquelle on associe des fonctions qu'on appelle méthodes.

```c++
class Rectangle {
  public:
    void set_values (int,int);
    int area (void);

private:
    int m_width;
    int m_height;
};

```

```c++
#include <iostream>
#include <iomanip>
using namespace std;

class Rectangle {
public:
    void set_values (int w,int h)
    {
        // on peux accéder à tout les champs et méthodes définis dans la classe
        m_width = w;
        m_height = h;

        // on peut aussi utiliser un pointeur vers l'object courant appelé `this`
        // this->m_height = w;
        // this->m_height = h;
    }
    int area (void)
    {
        return m_width * m_height;
    }

private:
    // on ne peux
    int m_width;
    int m_height;
};

int main()
{
    Rectangle rect;
    // rect.m_width n'est pas accessible car privé

    rect.set_values(5,2);
    cout << "rect area: " <<  rect.area() << endl;

    return 0;
}

```

Généralement on défini les méthodes séparément de leur déclaration dans la classe. On distingue ces 2 fichiers en `.h` ou `.hpp` pour header et `.cpp` ou `.cc` pour la définition.

QtCreator peut nous crééer ces 2 fichiers pour nous en selectionnant nouveau `Class C++`. Nommé votre classe `Rectangle`. Puis dans le CMakeList.txt ajouter `rectangle.cpp` à `add_executable(nom_projet main.cpp rectangle.cpp)`.

```c++
// rectangle.h
#ifndef RECTANGLE_H
#define RECTANGLE_H


class Rectangle
{
public:
    Rectangle();

    void set_values (int,int); // nommé les paramètre est facultatif à la déclaration
    int area (void);

private:
    int m_width; // largeur
    int m_height; // heuteur
};

#endif // RECTANGLE_H
```

```c++
// rectangle.cpp
#include "rectangle.h"

Rectangle::Rectangle()
{

}

void Rectangle::set_values(int w, int h)
{
    // on peux accéder à tout les champs et méthodes définis dans la classe
    m_width = w;
    m_height = h;

    // on peut aussi utiliser un pointeur vers l'object courant appelé `this`
    // this->m_height = w;
    // this->m_height = h;
}
int Rectangle::area (void)
{
    return m_width * m_height;
}
```

```c++
// main.cpp
#include <iostream>
#include "rectangle.h"

using namespace std;

int main()
{
    Rectangle rect;
    // rect.m_width n'est pas accessible car privé

    rect.set_values(5,2);
    cout << "rect area: " <<  rect.area() << endl;

    return 0;
}
```

### Constructeurs / Destructeur

Les constructeurs sont des méthodes particulières qui n'ont pas de type de retour et portent le même nom de la classe et initialise l'objet instancié.

Le descructeur est appeler `~Rectangle` lorsqu'un variable de type `Rectangle` sort de scope ou est `delete` s'il à été instantier par `new`. Le destructeur permet de liberé la mémoire éventuelement allouer lors de l'existance de l'objets, souvent en appelant le destruccteur de ses membres.

```c++
#include <iostream>
using namespace std;

class Rectangle {
public:
    Rectangle(int w,int h) : m_width(w), m_height(h)
    {
        cout << "constructeur 2 params" << endl;
    }

    Rectangle(int c) : m_width(c), m_height(c)
    {
        cout << "constructeur 1 param" << endl;
    }

    Rectangle() : m_width(0), m_height(0)
    {
        cout << "constructeur vide" << endl;
    }
    // constructeur par copie
    Rectangle(const Rectangle &r) : m_width(r.m_width), m_height(r.m_height)
    {
        cout << "constructeur par copie" << endl;
    }

    ~Rectangle()
    {
        cout << "destructeur Rectange " << m_width << ',' << m_height << endl;
    }

    // const car ne modifie pas la classe
    // getter
    int width() const { return m_width; }
    int height() const { return m_height; }

    // setter
    void width(int w) { m_width = w; }
    void height(int h)  { m_height = h; }
private:
    int m_width;
    int m_height;
};

int main()
{
    Rectangle rect(4,2);
    Rectangle r1(rect);

    cout << "rect " << rect.width() << ',' << rect.height() << endl;
    cout << "r1   " << r1.width() << ',' << r1.height() << endl;

    r1.width(3);

    cout << "rect " << rect.width() << ',' << rect.height() << endl;
    cout << "r1   " << r1.width() << ',' << r1.height() << endl;

    {
        Rectangle r(1);
        Rectangle r2(2);

        // desctructeur appeler ici
    }

    Rectangle *ptr = new Rectangle(42);

    Rectangle r3(3);
    delete ptr;
    Rectangle r4;

    return 0;
}

```

### Sucharge d'opérateurs

En c++ les opérateur du langage peuvent être redéfini par vos classe.

```c++
#include <iostream>

using namespace std;

class Point {
public:
    // constructeurs
    Point(int x = 0, int y = 0): m_x(x), m_y(y) {}
    Point(const Point & p): m_x(p.m_x), m_y(p.m_y) {}

    // permet d'utiliser un object Point directement dans cout <<
    friend std::ostream& operator<< (std::ostream& out, Point const& p) {
        out << '(' << p.m_x << ',' << p.m_y << ')';
        return out;
    }

    // surgarge de l'opérateur d'égalité
    bool operator==(Point const& p) const {
        return this->m_x == p.m_x && this->m_y == p.m_y;
    }

    // surgarge de l'opérateur d'inégalité
    bool operator!=(Point const& p) const {
        return !(*this == p);
    }

    // surgarge du + entre 2 points (this, soit même) et un autre
    Point operator+ (const Point& p) const {
        return Point(this->m_x + p.m_x, this->m_y + p.m_y);
    }

    // surgarge du * mais définit hors de la classe
    // friend permet d'accéder au membre privé de la classe hors de celle ci
    friend Point operator* (const Point& p, const int scale);

private:
    int m_x;
    int m_y;
};

// pas de friend ici mais peux utiliser les m_x et m_y grâce au friend défini dans la classe
Point operator* (const Point& p, const int scale) {
    return Point(p.m_x * scale, p.m_y * scale);
}

// on l'ordre des paramètres est important (int * Point) != (Point * int)
// ici on effectue la même opération peut importe l'ordre mais on pourrait définir 
// un comportement différent.
Point operator* (int scale, const Point& p) {
    return p * scale;
}

int main()
{
    Point a(1,2);
    Point b(4,5);

    cout << a << " + " << b << " = " << (a + b) << endl;

    cout << a << " * " << 3 << " = " << (a * 3) << endl;
    cout << 3 << " * " << a << " = " << (3 * a) << endl;

    // n'est possible qu'avec la surcharge de ==
    if (a == b) {
        cout << a << " == " << b << endl;
    } else {
        cout << a << " != " << b << endl;
    }

    // n'est possible qu'avec la surcharge de !=
    if (a != b) {
        cout << a << " != " << b << endl;
    } else {
        cout << a << " == " << b << endl;
    }

    return 0;
}
```

Ecrire la surcharge d'autre opérateur, `-`, `/`, `[]`, où `[0]` retourn x et `[1]` retourne y.

## Héritage

```c++
#include <iostream>
#include <vector>

using namespace std;

class Rectangle {
public:
    Rectangle() : m_width(0), m_height(0) {}
    Rectangle(int w, int h) : m_width(w), m_height(h) {}
    Rectangle(const Rectangle& r) : m_width(r.m_width), m_height(r.m_height) {}

    string name() const { return "rectangle"; }

    friend std::ostream& operator<< (std::ostream& out, Rectangle const& p) {
        out << p.m_width << 'x' << p.m_height;
        return out;
    }

    int area() const { return m_width * m_height; }
    int perimeter() const { return (m_width + m_height) * 2; }

protected: // pouvant être accessible par les classes filles
    int m_width;
    int m_height;
};


class Square : public Rectangle {
public:
    Square() : Square(0) {}
    Square(int c) : Rectangle(c, c) {}
    Square(const Square& s): Square(s.m_width) {}

    // redefini name
    string name() const { return "square"; }

    // friend std::ostream& operator<< (std::ostream& out, Square const& p) {
    //     out << '[' << p.m_width << ']';
    //     return out;
    // }
};

class Circle {
public:
    Circle(int r): m_radius(r) {}

    string name() const { return "circle"; }

    int area() const { return (int)((float)(m_radius * m_radius) * 3.14); }
    int perimeter() const { return (int)((float)(m_radius * 2) * 3.14); }

    friend std::ostream& operator<< (std::ostream& out, Circle const& p) {
        out << '(' << p.m_radius << ')';
        return out;
    }
private:
    

int main()
{
    Rectangle r(2,5);
    cout << r.name() << "\t" << r << "\t area: " << r.area() << "\t perimetter: " << r.perimeter() << endl;

    Square s(3);
    cout << s.name() << "\t" << s << "\t area: " << s.area() << "\t perimetter: " << s.perimeter() << endl;

    Circle c(10);
    cout << c.name() << "\t" << c << "\t area: " << c.area() << "\t perimetter: " << c.perimeter() << endl;

    return 0;
}
```

Un `Square` est  un `Rectangle` il hérite de tout ses membres et méthodes. Il peux les surcharger s'il le veux.

Décommenter la méthode `friend std::ostream& operator<< (std::ostream& out, Square const& p)` de Square et réexecuté.

Ajouter à la fin de votre main:

```c++
    Rectangle *rect = &s;
    cout << rect->name() << "\t" << *rect << "\t area: " << rect->area() << "\t perimetter: "<< rect->perimeter() << endl;
```

### Classe abstraite

Ajouter avant la class Rectangle et changer les definitions des classes `Rectangle` et `Circle` pour ajouter `: public Shape` :

```c++
class Shape {
public:
    virtual string name() const { return "shape"; }
    virtual int area() const = 0;
    virtual int perimeter() const = 0;
};

class Rectangle : public Shape {
// ...
class Circle : public Shape {
// ...
```

Dans votre main ajoutez

```c++
    vector<Shape*> shapes;
    shapes.push_back(&r);
    shapes.push_back(&s);
    shapes.push_back(&c);

    for (Shape *shape: shapes) {
        cout << shape->name() << "\t" << "\t area: " << shape->area() << "\t perimetter: " << shape->perimeter() << endl;
    }
```
