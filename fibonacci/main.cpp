#include <iostream>
#include <vector>

using namespace std;

int main() {
  // the two last numbers in the list are declared as separate variables
  // they will be reassigned as we go
  int penultimate = 1;
  int ultimate = 2;

  // allocate memory for the sum right away
  int sum;

  // the fibonnacci suite is a vector of integers
  vector<int> fibonacci_suite;

  fibonacci_suite.push_back(penultimate);
  fibonacci_suite.push_back(ultimate);

  // in Rust we couldn't start a loop with an uninstanciated sum variable
  // but here we are
  for (;;) {
    sum = penultimate + ultimate;

    if (sum > 1000) {
      break;
    }

    fibonacci_suite.push_back(sum);

    penultimate = ultimate;
    ultimate = sum;
  }

  cout << "The Fibonnacci suite up to 1000: " << std::endl;
  for (int i = 0; i < fibonacci_suite.size(); i++)
    cout << fibonacci_suite[i] << ", ";
  cout << "c est tout !" << std::endl;
}