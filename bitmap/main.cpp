#include <cstdint>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <stdio.h>

using namespace std;

struct BitmapFileHeader {
  //  standard values WAIT THAT'S LITTLE ENDIAN
  uint16_t bfType;      //  42 4D
  uint32_t bfSize;      //  00 00 00 00
  uint16_t bfReserved1; //  00 00
  uint16_t bfReserved2; //  00 00
  uint32_t bfOffBits;   //  36 00 00 00
};

struct BitmapInfoHeader {
  uint32_t biSize;   // 28 00 00 00
  uint32_t biWidth;  // 00 00 00 00
  uint32_t biHeight; // 00 00 00 00

  uint16_t biPlanes;   // 01 00
  uint16_t biBitCount; // 00 00

  uint32_t biCompression;   // 00 00 00 00
  uint32_t biSizeImage;     // 00 00 00 00
  uint32_t biXpelsPerMeter; // 00 00 00 00
  uint32_t biYpelsPerMeter; // 00 00 00 00
  uint32_t biClrUsed;       // 00 00 00 00
  uint32_t biClrImportant;  // 00 00 00 00
};

// a color is made of three bytes, one for each RGB value
// IN THIS ORDER: BLUE GREEN RED (for bitmap only)
struct Color {
  uint8_t blue;
  uint8_t green;
  uint8_t red;
};

int main() {

  const int HEIGHT = 600;
  const int WIDTH = 900;
  Color flag_grid[HEIGHT][WIDTH];

  // fill the grid with the colors of germany
  for (int y = 0; y < HEIGHT; y++) {
    for (int x = 0; x < WIDTH; x++) {
      Color color_to_fill;

      // Black stripe #000000
      if (400 < y) {
        color_to_fill.red = 0;
        color_to_fill.green = 0;
        color_to_fill.blue = 0;
      }

      // Red stripe #FF000
      else if (200 < y && y < 400) {
        color_to_fill.red = 0xFF;
        color_to_fill.green = 0;
        color_to_fill.blue = 0;
      }

      // Gold stripe #FFCC00
      else if (y < 200) {
        color_to_fill.red = 0xFF;
        color_to_fill.green = 0xCC;
        color_to_fill.blue = 0;
      }

      flag_grid[y][x] = color_to_fill;
    }
  }

  BitmapFileHeader bitmap_file_header;
  BitmapInfoHeader bitmap_info_header;

  // set values in the header according to the image

  // BITMAPFILEHEADER
  bitmap_file_header.bfType = 0x4D42; // it appears as 42 4D
  bitmap_file_header.bfSize = 14      // size of the bitmap file header
                              + 40    // size of the bitmap info header
                              + sizeof(flag_grid);
  bitmap_file_header.bfReserved1 = 0;  // 00 00
  bitmap_file_header.bfReserved2 = 0;  // 00 00
  bitmap_file_header.bfOffBits = 0x36; // 36 00 00 00

  // BITMAPINFOHEADER
  bitmap_info_header.biSize = 0x28;       // 28 00 00 00 = 40
  bitmap_info_header.biWidth = 0;         // 00 00 00 00
  bitmap_info_header.biHeight = 0;        // 00 00 00 00
  bitmap_info_header.biPlanes = 0x01;     // 01 00
  bitmap_info_header.biBitCount = 0;      // 00 00
  bitmap_info_header.biCompression = 0;   // 00 00 00 00
  bitmap_info_header.biSizeImage = 0;     // 00 00 00 00
  bitmap_info_header.biXpelsPerMeter = 0; // 00 00 00 00
  bitmap_info_header.biYpelsPerMeter = 0; // 00 00 00 00
  bitmap_info_header.biClrUsed = 0;       // 00 00 00 00
  bitmap_info_header.biClrImportant = 0;  // 00 00 00 00

  cout << "size of the bitmap file header: " << sizeof(bitmap_file_header)
       << endl;
  cout << "size of the bitmap info header: " << sizeof(bitmap_info_header)
       << endl;
  cout << "size of the grid: " << sizeof(flag_grid) << endl;

  bitmap_info_header.biWidth = WIDTH;
  bitmap_info_header.biHeight = HEIGHT;
  bitmap_info_header.biBitCount = 24; // 24 bits per pixel 0x1100
  bitmap_info_header.biSizeImage = sizeof(flag_grid);

  // create the file
  FILE *myfile = fopen("image.bmp", "w");

  //   this C++ thing adds two bytes to the bitmap file header if I try to write
  //   it in one go. *sigh* Why, C, why?
  fwrite(&bitmap_file_header.bfType, 2, 1, myfile);
  fwrite(&bitmap_file_header.bfSize, 4, 1, myfile);
  fwrite(&bitmap_file_header.bfReserved1, 2, 1, myfile);
  fwrite(&bitmap_file_header.bfReserved2, 2, 1, myfile);
  fwrite(&bitmap_file_header.bfOffBits, 4, 1, myfile);

  fwrite(&bitmap_info_header, sizeof(bitmap_info_header), 1, myfile);
  fwrite(&flag_grid, sizeof(flag_grid), 1, myfile);

  fclose(myfile);

  return 0;
}