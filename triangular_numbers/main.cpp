#include <iostream>
#include <vector>

using namespace std;

int compute_triangular_number(int n) { return (n * (n + 1)) / 2; }

// this finds all triangular number up to 100
int main() {

  // the index to be incremented
  int n = 1;

  // allocate the memory for the computed triangular number
  int triangular_number;

  vector<int> triangular_numbers;

  for (;;) {
    triangular_number = compute_triangular_number(n);

    if (triangular_number > 100) {
      break;
    }

    triangular_numbers.push_back(triangular_number);

    n++;
  }

  cout << "Les nombres triangulaires jusqu'à 100: " << std::endl;
  for (int i = 0; i < triangular_numbers.size(); i++)
    cout << triangular_numbers[i] << ", ";
  cout << std::endl;
  cout << "c'est tout !" << std::endl;
}
