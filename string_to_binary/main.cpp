#include <iostream>
#include <math.h>
using namespace std;

// takes a uint32_t
std::string to_binary(uint32_t number) {
  std::string binary_string;

  for (int power_of_two = 31; power_of_two >= 0; power_of_two -= 1) {
    if (number / pow(2, power_of_two) >= 1) {
      binary_string.append("1");
      number = number - pow(2, power_of_two);
    } else {
      binary_string.append("0");
    }
  }

  return binary_string;
}

int main() {

  // get a input from stdin
  uint32_t number;
  cout << "type a number:";
  cin >> number;

  // detect wrong input
  if (cin.fail()) {
    cout << "bad input!" << endl;
    cin.clear();
    cin.ignore();
  }

  std::string converted_number;
  converted_number = to_binary(number);

  cout << number << " in binary is: " << converted_number << endl;
}