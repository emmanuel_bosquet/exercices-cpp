# Apprendre Qt en C++ avec Thomas Moinel

Deux jours de C++, deux jours de Qt.

## Comment écrire / compiler du C++ sans Qt Creator

Dans un dossier, écrire un `main.cpp`, et faire :

    g++ main.cpp

Ça génère un binaire `a.out`.

    ./a.out

Et voilà.


