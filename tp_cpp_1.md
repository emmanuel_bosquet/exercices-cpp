# TP 1 C++

## 1. Fibonnacci

Ecrire un programme qui affiche la [suite de fibonacci](https://fr.wikipedia.org/wiki/Suite_de_Fibonacci) inferieur à `1000`.

## 2. Nombre triangulaire

Ecrire un programme qui affiche tout les [nombres triangulaires](https://fr.wikipedia.org/wiki/Nombre_triangulaire) inferieur à `100`.

## 3. Jeux trouver le nombre caché entre 1 et 100

utiliser `random` pour génrer un nombre aléatoire, `cin` pour demander à l'utilisateur de deviner le nombre. Afficher `trop petit`, `trop grand`, ou `victore !`.

```c++
#include <random>
//...

    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(1, 100); // define the range
```

## 4. Affichage binaire

Il n'existe pas de fonction en c++ pour afficher des nombres en binaire, ecrire une telle fonction dont le prototype est le suivant.

```c++
std::string to_binary(int val);
```

## 5. Fréquence des lettres des mots du dictionnaires

Ecrire un programme qui lit le fichier [dictionnaire.txt](https://tomu.fr/ecole404/dictionnaire) contenant 1 mot par ligne et affiche pour chaque lettre de l'alphabet son nombre d'aparition sous le format suivant :

```
lettre, fréquence
a, 12155
b, 3104
...
```

## 6. hexdump

Ecrire une copie de hexdump, qui prend en paramettre une fichier et affiche ca représentation en hexadecimal ainsi que les caracatères ascii affichable sous forme:

Adresse en hexadecimal du 1er octet de la ligne, 2 _ 8 _ octets en hexadecimal, | représentation ascii |

```
00000000  42 4d 00 00 00 00 00 00  00 00 36 00 00 00 28 00  |BM........6...(.|
00000010  00 00 84 03 00 00 58 02  00 00 01 00 18 00 00 00  |......X.........|
00000020  00 00 b6 f9 20 00 00 00  00 00 00 00 00 00 00 00  |.... ...........|
00000030  ff ff ff ff ff ff ff ff  ff                       |.........       |
```

Les caractère à remplacer par un point `'.'` sont ceux dont la valeur est inférieur à 32 ou supérieur à 127.

Il faut ouvrir le fichier en binaire et utiliser `#include <iomanip>` avec `std::cout << std::setfill('0') << std::setw(2) << std::hex << byte;` pour afficher un nombre en hexadecimal.

## 7. Drapeaux BMP

Ecrire un programme qui ecrit un fichier image `drapeau.bmp` au format BMP d'une taille de 900x600 sans compression d'un drapeau tricolor au choix.

Documentation du [format bmp](https://www.apprendre-en-ligne.net/info/images/formatbmp.pdf)

-   Créer un tableau 2d `uint32_t`
-   remplissez le avec les couleurs du drapeaux,
    chaque pixel utilsant le représentation des couleurs hexadecimal souvent utilsier en web `#f20045` => `0xf20045` ou `f2` est la composante rouge sur 8bits, `00`, vert et `45` bleu.
-   Créer une `struct BMPHeader` avec tout les champs de l'entête de la spec BMP.
-   Ecrire cette `struct` dans un fichier après y avoir mis les bonnes valeurs selon la spécification.
-   convertir le tableau de pixel en extrayant les 3 composantes `r`, `g`, `b` du pixel `uint32_t`.
-   Vous pouvez férifier l'entête de votre fichier grâce à votre `hexdump`.
-   Chercher et Utiliser `reinterpret_cast<char*>(val)` dans la methode `write` de votre fichier.
